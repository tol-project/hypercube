<?php
//HyperCubeBrowser
//CHECKOUT FROM SVN in htdocs
//svn checkout https://www.tol-project.org/svn/tolp/OfficialTolArchiveNetwork/HyperCube/HyperCubeBrowser HyperCubeBrowser

$tabEngine = 'GoogleVisualization';

  include_once "general.php";
//<script type="text/javascript" src="./jsapi.js"></script>
//<script type="text/javascript" src="./uds_api_contents.js"></script>
?>

<html>
<head>
  <!-- meta charset="UTF-8" -->
  <title>TOL Hypercube (GVT)</title>
  <link rel="stylesheet" href="./style.css" type="text/css" media="screen" charset="utf-8">
  <script type='text/javascript' src='http://www.google.com/jsapi' ></script>
  <script type="text/javascript" src="./jquery-1.11.3.min.js" ></script>
  <script type="text/javascript" src="./js-csv-encoding-master/encoding-indexes.js"></script>
  <script type="text/javascript" src="./js-csv-encoding-master/encoding.js"></script>
  <script type="text/javascript" src="./js-csv-encoding-master/FileSaver.js"></script>
  <script type="text/javascript" src="./exportAsCsv.js"></script>
  
  <!-- script type="text/javascript"src="./export.js" charset="utf-8"></script -->

<script type='text/javascript'>
google.load('visualization', '1', {'packages':['annotatedtimeline','table']});
google.setOnLoadCallback(createDrawers);

var info_data = '';
var info_view = '';
var datasets_data = '';
var datasets_view = '';
var dimensions_data = '';
var dimensions_view = '';
var keyvalues_data = '';
var keyvalues_view = '';
var summary_data = '';
var summary_view = '';
var series_data = '';
var series_view = '';
var chart_drawer;
var table_drawer;
var datasets_drawer = '';
var dimensions_drawer = '';
var keyvalues_drawer = '';
var summary_drawer = '';
var datasets_selIdx = [];
var keyvalues_selIdx = [];
var summary_selIdx = [];
var datasets_sel = [];
var keyvalues_sel = [];
var summary_sel = [];
var current_csv_prefix = '';
var datasets = '';
var keyValues = '';
var serCodes = '';

var datasetDimKeySelect = 
"SELECT co_dataset, co_dimension, co_key "+
"FROM def_serie_key "+
"GROUP BY co_dataset, co_dimension, co_key "+
"ORDER BY co_dataset, co_dimension, co_key; ";
var datasets_select = "SELECT "+
      "co_dataset as Name,"+
      "co_source as Source,"+
      "co_magnitude ||\' [\'||co_units_tol||\']\' as Magnitude,"+
      "co_base_dating as Freq, "+
      "replace(te_dimensions,',',', ') as Dimensions,"+
      "te_description as Description "+
      "FROM def_dataset "+
      "ORDER BY id_dataset";
var dimensions_select = "SELECT "+
      "co_dimension as Name,"+
      "te_desc as Description "+
      "FROM def_dimension "+
      "ORDER BY co_dimension";        

var keyvalues_select = "SELECT "+
      "co_dimension as Dimension,"+
      "co_name as Code, "+
      "co_key as Code, "+
      "te_description as Description "+
      "FROM def_key "+
      "ORDER BY co_dimension,co_key";


 
/*
 new Dygraph(
        document.getElementById("noroll"),
        data_temp,
        {
          customBars: true,
          title: 'Daily Temperatures in New York vs. San Francisco',
          ylabel: 'Temperature (F)',
          legend: 'always',
          labelsDivStyles: { 'textAlign': 'right' },
          showRangeSelector: true
        }
    );
*/        
function buildHyperCubeData()
{
  datasets_data  = new google.visualization.DataTable($.ajax({
    url: "./dbTabJson.php?dbtask=query&dbquery="+encodeURIComponent(
      datasets_select),
    contentType: "charset=utf-8", 
    dataType:'json',async: false}).responseText);
  datasets_view = new google.visualization.DataView(datasets_data);        
  dimensions_data = new google.visualization.DataTable($.ajax({
    url: "./dbTabJson.php?dbtask=query&dbquery="+encodeURIComponent(
      dimensions_select),
    contentType: "charset=utf-8", 
    dataType:'json',async: false}).responseText);
  dimensions_view = new google.visualization.DataView(dimensions_data);        
  keyvalues_data = new google.visualization.DataTable($.ajax({
    url: "./dbTabJson.php?dbtask=query&dbquery="+encodeURIComponent(
      keyvalues_select),
    contentType: "charset=utf-8", 
    dataType:'json',async: false}).responseText);
  keyvalues_view = new google.visualization.DataView(keyvalues_data);      
  datasetDimKey  = new google.visualization.DataTable($.ajax({
    url: './dbTabJson.php?dbtask=query&dbquery='+encodeURIComponent(
      datasetDimKeySelect),
    contentType: 'charset=utf-8', 
    dataType:'json',async: false}).responseText);
  1
}  

function datasets_drawer_selectHandler(e) 
{
  var selection = datasets_drawer.getSelection();
  var message = '';
  datasets_sel = [];
  datasets_selIdx = [];
  if(selection.length==0)
  {  
  //message = 'No dimensions has been selected';
    dimensions_view.setRows(0, dimensions_data.getNumberOfRows()-1);  
    keyvalues_view .setRows(0, keyvalues_data .getNumberOfRows()-1); 
  //alert("You must select almost one dataset to select series!")     
  }
  else
  {
  //message = selection.length+' dimensions has been selected';
    var filter_dim = [];
    var filter_key = [];
    for ( i = 0; i < selection.length; i++) 
    {
      var str, i, j;
      var item = selection[i];
      datasets_selIdx[i] = item.row;
      datasets_sel[i] = datasets_data.getFormattedValue(item.row, 0);
      var ds = datasets_sel[i];
      var flt_dimKey = datasetDimKey.getFilteredRows([{column: 0, value: ds}] )
      //message += '  ['+i+'] '+ds+' -> '+JSON.stringify(flt_dimKey)+"\n";
      for(j = 0; j < flt_dimKey.length; j++) 
      {
        var co_dim = datasetDimKey.getFormattedValue(flt_dimKey[j], 1);
        var co_key = datasetDimKey.getFormattedValue(flt_dimKey[j], 2);
      //message += '{'+co_dim+','+co_key+'},';
        var flt_dim = dimensions_data.getFilteredRows([{column: 0, value: co_dim}]);
        var flt_key = keyvalues_data .getFilteredRows([{column: 0, value: co_dim},{column: 2, value: co_key}]);
      //message += '  ['+i+','+j+'] '+JSON.stringify(dataset_dimensions[item.row][j])+' -> '+JSON.stringify(flt_key)+"\n";
        filter_dim = filter_dim.concat(flt_dim).unique();
        filter_key = filter_key.concat(flt_key).unique();
      }
/*
      for(j = 0; j < dataset_dimensions[item.row].length; j++) 
      {
        var flt_dim = dimensions_data.getFilteredRows(dataset_dimensions[item.row][j]);
        var flt_key = keyvalues_data .getFilteredRows(dataset_dimensions[item.row][j]);
      //message += '  ['+i+','+j+'] '+JSON.stringify(dataset_dimensions[item.row][j])+' -> '+JSON.stringify(flt_key)+"\n";
        filter_dim = filter_dim.concat(flt_dim).unique();
        filter_key = filter_key.concat(flt_key).unique();
      }
*/        
    }
  //message += '\nfilter:\n'+JSON.stringify(filter);
    dimensions_view.setRows(filter_dim);
    keyvalues_view .setRows(filter_key);
  }
//alert(message);
  if(summary_data!='')
  { summary_view.setRows(0, summary_data.getNumberOfRows()-1); }
  if(series_data!='')
  { series_view.setRows(0, series_data.getNumberOfRows()-1); }
}
    
function keyvalues_drawer_selectHandler(e) 
{
  var selection = keyvalues_drawer.getSelection();
  keyvalues_sel = [];
  for (var i = 0; i < selection.length; i++) 
  {
    var item = selection[i];
    keyvalues_selIdx[i] = item.row;
    keyvalues_sel[i] = 
      keyvalues_view.getFormattedValue(item.row, 0)+'.'+
      keyvalues_view.getFormattedValue(item.row, 2);
  }
}     

function summary_drawer_selectHandler(e) 
{
  var selection = summary_drawer.getSelection();
  summary_sel = [];
  for (var i = 0; i < selection.length; i++) 
  {
    var item = selection[i];
    summary_selIdx[i] = item.row;
    summary_sel[i] = summary_view.getFormattedValue(item.row, 0);
  }
}     

function getSelectedDataSets() 
{ 
  var lst = "";
  datasets_drawer_selectHandler('');
  for(i=0; i<datasets_sel.length; i++)
  {
    if(i>0) { lst += ","; }
    lst += "'"+datasets_sel[i]+"'";
  };
  return(lst);
};

function getSelectedKeyValues() 
{ 
  var lst = "";
  keyvalues_drawer_selectHandler('');
  for(i=0; i<keyvalues_sel.length; i++)
  {
    if(i>0) { lst += ","; }
    lst += "'"+keyvalues_sel[i]+"'";
  };
  return(lst);
};
 
function getSelectedSeries() 
{ 
  var lst = "";
  summary_drawer_selectHandler('');
  for(i=0; i<summary_sel.length; i++)
  {
    if(i>0) { lst += ","; }
    lst += "'"+summary_sel[i]+"'";
  };
  return(lst);
};


function checkCrossSelection()
{
  datasets = getSelectedDataSets();
  keyValues = getSelectedKeyValues();
  if((datasets.length==0)||(keyValues.length==0))
  {
    if((datasets.length==0)&&(keyValues.length==0))
    {
      alert('Sorry: You must select at least one or more datasets and one or more key values')
      datasets_click();
    }
    else if((datasets.length==0)&&(keyValues.length>0))
    {
      alert('Sorry: You must select at least one or more datasets')
      datasets_click();
    }
    else if((datasets.length>0)&&(keyValues.length==0))
    {
      alert('Sorry: You must select at least one or more key values')
      keyvalues_click();
    }
    return(0);
  }
  else
  {
    return(1);
  }
};

function checkSummarySelection()
{
  serCodes = getSelectedSeries();
  return(serCodes.length>0);
};

function buildSeriesSummary()
{
  if(checkCrossSelection())
  {
    var url_ = "./dbTabJson.php?dbtask=seriesSummary"+
    "&datasets="+encodeURIComponent(datasets)+
    "&keyValues="+encodeURIComponent(keyValues);
//  document.write(url_);
    var json_txt = $.ajax({
      url: url_,
      contentType: "charset=utf-8", 
      dataType:'json',async: false}).responseText;
  //document.write(json_txt);
    summary_data = new google.visualization.DataTable(json_txt);
    summary_view = new google.visualization.DataView(summary_data);        
  //alert("TRACE summary_data='"+summary_data+"'\n");
    return(1);
  }
  else
  {
    return(0);
  }
} 

function buildSeriesData()
{
  var url_ = '';
  if(checkSummarySelection())
  {
    url_ = "./dbTabJson.php?dbtask=seriesDataFromCodes"+
    "&serCodes="+serCodes;
  }
  else if(checkCrossSelection())
  {
    url_ = "./dbTabJson.php?dbtask=seriesDataFromCross"+
    "&datasets="+encodeURIComponent(datasets)+
    "&keyValues="+encodeURIComponent(keyValues);
  }
//document.write(url_);
  if(url_=='') { return(0); }
  else
  {
    var json_txt = $.ajax({
      url: url_,
      contentType: "charset=utf-8", 
      dataType:'json',async: false}).responseText;
  //document.write(json_txt);
    series_data = new google.visualization.DataTable(json_txt);
    series_view = new google.visualization.DataView(series_data);        
    return(1);
  }
}  

function createDrawers()
{
  buildHyperCubeData();
  datasets_drawer   = new google.visualization.Table(document.getElementById('datasets_view'));
  dimensions_drawer = new google.visualization.Table(document.getElementById('dimensions_view'));
  keyvalues_drawer  = new google.visualization.Table(document.getElementById('keyvalues_view'));
  summary_drawer    = new google.visualization.Table(document.getElementById('summary_view'));
  table_drawer      = new google.visualization.Table(document.getElementById('table_view'));
  chart_drawer      = new google.visualization.AnnotatedTimeLine(document.getElementById('chart_view'));
  // Add our selection handler.
  //google.visualization.events.addListener(datasets_drawer,  'select', datasets_drawer_selectHandler);
  //google.visualization.events.addListener(keyvalues_drawer, 'select', keyvalues_drawer_selectHandler);
  1
};

function draw_info() 
{
};

function draw_a_table(drawer, data, sortMode) 
{
  if(sortMode==1)
  {
    drawer.draw(data, {
      alternatingRowStyle:false, 
      showRowNumber: false, 
      sortColumn:0, 
      sortAscending:true, 
      page:'disable'})
  } else if(sortMode==-1)
  {
    drawer.draw(data, {
      alternatingRowStyle:false, 
      showRowNumber: false, 
      sortColumn:0, 
      sortAscending:false, 
      page:'disable'})
  } else if(sortMode==0)
  {
    drawer.draw(data, {
      alternatingRowStyle:false, 
      showRowNumber: true, 
      page:'disable'})
  } 
};

function draw_datasets() 
{ 
  dimensions_view.setRows(datasets_selIdx);
  draw_a_table(datasets_drawer,datasets_view,0);   
};

function draw_dimensions() 
{ 
  datasets_drawer_selectHandler('');
  draw_a_table(dimensions_drawer,dimensions_view,0); 
};

function draw_keyvalues() 
{ 
  keyvalues_view.setRows(keyvalues_selIdx);
  datasets_drawer_selectHandler('');
  draw_a_table(keyvalues_drawer,keyvalues_view,0);     
};

function draw_summary() 
{ 
  //keyvalues_drawer_selectHandler('');
  var ok = buildSeriesSummary(); 
  if(ok>0) { draw_a_table(summary_drawer,summary_view,0); }
};

function draw_table() 
{ 
  //keyvalues_drawer_selectHandler('');
  var ok = buildSeriesData();
  if(ok>0) { draw_a_table(table_drawer,series_view,0); }
};

function draw_chart() 
{
  //keyvalues_drawer_selectHandler('');
  var ok = buildSeriesData();
  if(ok>0) { 
  chart_drawer.draw(series_data, {
    displayAnnotations: true,
    displayAnnotationsFilter : true,
    displayExactValues: false,
    displayLegendValues: true,
    displayZoomButtons: true,
    legendPosition: 'newRow',
    fill: 0,
    annotationsWidth : 50,
    scaleType: 'fixed',
    thickness: 1
  });  }
};


function clean_click() 
{
  if(summary_data!='')
  { 
    summary_view. setRows(0, 0); 
    summary_view. setRows(0, summary_data   .getNumberOfRows()-1); 
  }
  if(series_data!='')
  { 
    series_view.  setRows(0, 0); 
    series_view.  setRows(0, series_data    .getNumberOfRows()-1); 
  }
  datasets_view  .setRows(0, 0);  
  datasets_view  .setRows(0, datasets_data  .getNumberOfRows()-1);  
  dimensions_view.setRows(0, 0);  
  dimensions_view.setRows(0, dimensions_data.getNumberOfRows()-1);  
  keyvalues_view .setRows(0, 0);  
  keyvalues_view .setRows(0, keyvalues_data .getNumberOfRows()-1);  
//  location.reload();
  datasets_click();
}
  
Array.prototype.unique = function() 
{
    var a = this.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j, 1);
        }
    }
    return a;
};
      
<?php
echo "  var drawers_div = [";
$draw_panel=array('info','datasets','dimensions','keyvalues','summary','chart','table');
$draw_panel_title=array(
  'TOL HyperCube Help',
  'Dataset Selection',
  'Description of Dimensions',
  'Key-Values Selection',
  'Series Summary',
  'Series Chart',
  'Series Table',
  'Series Statistics');

for ($j=0; $j < count($draw_panel); $j++)
{
  if($j>0) { echo ','; }
  echo "'".$draw_panel[$j]."'";
}  
echo "];\n\n";

for ($i=0; $i < count($draw_panel); $i++)
{
  $dpi = $draw_panel[$i];
  $dataPrefix = "";
  switch ($dpi) {
  case 'chart' : $dataPrefix = 'series'; break;
  case 'table' : $dataPrefix = 'series'; break;
  default      : $dataPrefix = $dpi; }
  
  echo "function ".$dpi."_click() {";
  echo "
  current_csv_prefix='".$dataPrefix."';
  ";
  for ($j=0; $j < count($draw_panel); $j++)
  {
    $dpj = $draw_panel[$j];
    echo "  document.getElementById('".$dpj."').style.display=";
    if($i==$j) { echo "'block';\n"; }
    else       { echo "'none';\n"; }
  }  
  echo "  if (typeof draw_".$dpi." == 'function') \n".
       "  { draw_".$dpi."(); }\n";    
  echo "};\n";
};

 
echo "  
</script>  
</head>
<body font: Arial, sans-serif;>
<div id='viewer' style='
    background-color: #ffffff; 
    width:".$width."px; 
    height:".$height."px; 
    webkit-border-radius: 10px;
    moz-border-radius: 10px;
    border-radius: 10px;
'>
";
  
for ($i=0; $i < count($draw_panel); $i++)
{
  echo "<div id='".$draw_panel[$i]."' style='width:".$width."px;height:".$height."px;display:";
  if($draw_panel[$i]=='info') { echo "block'>\n"; } else  { echo "none'>\n"; }
  echo "  <div id='".$draw_panel[$i]."_title' style='
    webkit-border-top-left-radius: 10px;
    moz-border-top-left-radius: 10px;
    border-top-left-radius: 10px;
    webkit-border-top-right-radius: 10px;
    moz-border-top-right-radius: 10px;
    border-top-right-radius: 10px;
    background-color:#C3DDEB;
    color:#000000;
    width:".$width."px;height:35px;'>\n";
  echo "    <table align='center' height=33><tr><td style='padding:2px 2px;font-family:Arial;font-weight:bold;font-size:24px;color:#2980b9'>".$draw_panel_title[$i]."</td></tr></table>\n";
  echo "  </div>\n";
  echo "  <div id='".$draw_panel[$i]."_view' style='width:".$width."px;height:".($height-35)."px'>";
  if($draw_panel[$i]=='info')
  {
    echo "    <div id='".$draw_panel[$i]."_html' style='width:".$width."px;height:".($height-35)."px;overflow:scroll'>";
    $a = file_get_contents("./info.php");
    echo ($a);
    echo "    </div>\n";
  }
  echo "  </div>\n";
  echo "</div>\n";
}
echo    
"
  <div id='commands' style='
    width:".$width."px; 
    height:40px;
    webkit-border-bottom-left-radius: 10px;
    moz-border-bottom-left-radius: 10px;
    border-bottom-left-radius: 10px;
    webkit-border-bottom-right-radius: 10px;
    moz-border-bottom-right-radius: 10px;
    border-bottom-right-radius: 10px;
    background-color:#C3DDEB;'>
    <table style='width:100%;border-spacing:0'> <tr>
      <td align='left'>
        <table><tr>          
          <th class='tg-031e'><button id='info_btn' type='button' class='btn' onclick='info_click()'style='display:block' >?</button></th>
          <th class='tg-031e'><button id='reports_btn' type='button' class='btn' onclick='reports_click()' style='display:block' >Reports</button></th>
          <th class='tg-031e'><button id='datasets_btn' type='button' class='btn' onclick='datasets_click()'style='display:block' >Dataset</button></th>
          <th class='tg-031e'><button id='dimensions_btn' type='button' class='btn' onclick='dimensions_click()' style='display:block' >Dim</button></th>
          <th class='tg-031e'><button id='keyvalues_btn' type='button' class='btn' onclick='keyvalues_click()' style='display:block' >Keys</button></th>
          <th class='tg-031e'><button id='clean_btn' type='button' class='btn' onclick='clean_click()' style='display:block' >Clean</button></th>
        </tr></table>
      </td>
      <td align='right'>
        <table><tr>
          <th class='tg-031e'><button id='summary_btn' type='button' class='btn' onclick='summary_click()' style='display:block' >Summary</button></th>
          <th class='tg-031e'><button id='chart_btn' type='button' class='btn' onclick='chart_click()' style='display:block' >Chart</button></th>
          <th class='tg-031e'><button id='table_btn' type='button' class='btn' onclick='table_click()' style='display:block' >Table</button></th>
          <th class='tg-031e'><button id='export_btn' type='button' class='btn' onclick='export_click()' style='display:block' >Export</button></th>
        </tr></table>
      </td>
    </tr> </table>
  </div>
</div>

<script type='text/javascript'>

</script>
</body>
</html>
";
?>  


  
  
  
