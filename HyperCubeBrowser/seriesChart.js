/////////////////////////////////////////////////////////////////////////////
//FILE: seriesChart.js
//
//PURPOSE: Defines Javascript class seriesChart to handle with time series
//charting tools.
//
// -Data are charged with Ajax from a given URL
// -Chart is drawn in a DIV created into a present DIV main container
// -User gives a name to identify all related HTML elements that will be
//  created automatically: 
//   * #<name>Div
// 
/////////////////////////////////////////////////////////////////////////////

document.write(
  '<script type="text/javascript" language="javascript" src="./dygraph-combined-dev.js" ></script>\n'+
  '<script type="text/javascript" language="javascript" src="./dygraph-extra.js" ></script>\n'
  );

var currentSeriesChart = '';
  
/////////////////////////////////////////////////////////////////////////////
var seriesChart = function  (args) 
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE new seriesChart');
//console.log(args);
  if (typeof(args.active)==='undefined') args.active = true;
//if (typeof(args.width)==='undefined') args.width = 800;
//if (typeof(args.height)==='undefined') args.height = 600;
  if (typeof(args.buttons)==='undefined') args.buttons = 
  ['csv','excel','copy','print'];
  if (typeof(args.enabledButtons)==='undefined') args.enabledButtons = [];
//console.log(args);
  this.active = args.active;
//this.width = args.width;
//this.height = args.height;
  this.container = args.container;
  this.name = args.name;
  this.url = args.url;
  this.buttons = args.buttons;
//console.log(this.url);
  this.divId = this.name+'Div';
  this.json = '';
  this.dygr = '';
  this.tab = '';
  this.propertyCheckArr = 
  [
    {id:"drawXGrid",              chkBox:'', checked:false, label:"X grid"      },
    {id:"drawYGrid",              chkBox:'', checked:true,  label:"Y grid"      },
    {id:"showRangeSelector",      chkBox:'', checked:false, label:"Time range"  },
    {id:"connectSeparatedPoints", chkBox:'', checked:true,  label:"Skip missed" },
    {id:"drawPoints",             chkBox:'', checked:false, label:"Draw points" }
  ];  
  this.propertyCheck = {};
  for(i=0; i<this.propertyCheckArr.length; i++)
  {
    var prp = this.propertyCheckArr[i];  
    this.propertyCheck[prp.id] = prp; 
  };    
  currentSeriesChart = this;
//console.log(this);
};

/////////////////////////////////////////////////////////////////////////////
seriesChart.prototype.setOption = function(id, value)
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE changeOption('+id+','+value+')');
  var expr = 'this.dygr.updateOptions({"'+id+'": '+value+'})';
//console.log('TRACE seriesChart.prototype.changeOption expr:"'+expr+'"');
  eval(expr);
};
  
/////////////////////////////////////////////////////////////////////////////
seriesChart.prototype.changeOption = function(id, value)
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE changeOption('+id+','+value+')');
  var divChart = document.getElementById(this.divId+'Chart');
  divChart.style.display = "none";
  this.setOption(id,value);
  this.dygr.resize();
  divChart.style.display = "block";
  this.dygr.resize();
};
  
/////////////////////////////////////////////////////////////////////////////
seriesChart.prototype.changeOptions = function(optLst)
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE changeOption('+id+','+value+')');
  var divChart = document.getElementById(this.divId+'Chart');
  divChart.style.display = "none";
  var expr = 'this.dygr.updateOptions(';
  for(i=0; i<optLst.length; i++)
  {
    if(i>0) { expr += ',\n'; }
    expr += '{"'+
      optLst[i].name+'": '+optLst[i].value+'}';
  }
  expr += ');';
//console.log('TRACE seriesChart.prototype.changeOptions expr:"'+expr+'"');
  eval(expr);
  this.dygr.resize();
  divChart.style.display = "block";
  this.dygr.resize();
};
  
 /* 
/////////////////////////////////////////////////////////////////////////////
seriesChart.prototype.propertyCheck_html = function()
/////////////////////////////////////////////////////////////////////////////
{
  var txt = ''; //'<table style="border=0;spacing=0;padding=0"> \n';
  for(i=0; i<this.propertyCheckArr.length; i++)
  {
    var prp = this.propertyCheckArr[i];  
    txt += 
    //'<tr><td>'+
    '<div class="checkbox">\n'+
    '<input type="checkbox" name="'+prp.id+'" id="chk_'+prp.id+'" '+ 
    (prp.checked?'checked':'') +
    ' onClick="currentSeriesChart.propertyCheck_click(\''+this.divId+'\')">'+
    '<label for="chk_'+prp.id+'">'+prp.label+' &nbsp</label>'+
    '</div> \n';
    //'</td></tr> \n';
  };
//txt += '</table> \n';
  return(txt); 
};  
*/
/////////////////////////////////////////////////////////////////////////////
seriesChart.prototype.propertyCheck_html = function()
/////////////////////////////////////////////////////////////////////////////
{
  var txt = ''; //'<table style="border=0;spacing=0;padding=0"> \n';
  for(i=0; i<this.propertyCheckArr.length; i++)
  {
    var prp = this.propertyCheckArr[i];  
    txt += 
    //'<tr><td>'+
    '<div class="checkbox">\n'+
    '<label>'+
    '<input type="checkbox" name="'+prp.id+'" id="chkBox_'+prp.id+'" '+ 
    (prp.checked?'checked':'') +
    ' onClick="currentSeriesChart.propertyCheck_click(\''+prp.id+'\')"> '+
    prp.label+
    '</label>'+
    '</div> \n';
    //'</td></tr> \n';
  };
//txt += '</table> \n';
  return(txt); 
};  

/////////////////////////////////////////////////////////////////////////////
seriesChart.prototype.propertyCheck_setElem = function(options)
/////////////////////////////////////////////////////////////////////////////
{
  for(i=0; i<this.propertyCheckArr.length; i++)
  {
    var prp = this.propertyCheckArr[i];  
    this.propertyCheckArr[i].chkBox = 
      document.getElementById('chkBox_'+prp.id);
  //console.log('TRACE propertyCheck_setElem id:'+prp.id)
  //console.log(this.propertyCheckArr[i].chkBox);
    options[prp.id] = prp.checked;
  }
};  

/////////////////////////////////////////////////////////////////////////////
seriesChart.prototype.propertyCheck_click = function(property)
/////////////////////////////////////////////////////////////////////////////
{
  var prp = this.propertyCheck[property];
//console.log('TRACE propertyCheck_click '+property);
//console.log(prp);
  var chkBox = prp.chkBox;
  prp.checked = chkBox.checked;
  this.changeOption(property,prp.checked);
};

/////////////////////////////////////////////////////////////////////////////
seriesChart.prototype.exportImageAsPng = function()
/////////////////////////////////////////////////////////////////////////////
{
  var canvas = Dygraph.Export.asCanvas(this.dygr);
  var src = canvas.toDataURL();
//console.log('TRACE seriesChart.prototype.exportImageAsPng: '+src);
  window.open(src,'Series Chart');
};

/////////////////////////////////////////////////////////////////////////////
seriesChart.prototype.html = function()
/////////////////////////////////////////////////////////////////////////////
{
  var div = document.getElementById(this.container);  
  var htm = 
  '<div id="'+this.divId+'" style="'+
  'width:100%; '+
  'height:100%; '+
  'display:'+(this.active?'block':'none')+
  '">\n';
  htm +=
  '<div  class="dt-buttons"'+
//'style="float:top; "'+
  '>\n';
  for(i=0; i<this.buttons.length; i++)
  {
    htm +=
    '<button class="dt-button" type="button" '+
    'onclick="clickPageButton(\''+this.buttons[i]+'\')">'+
    this.buttons[i]+'</button>';
  }  
  htm +=
  '<button class="dt-button" type="button" '+
  'onclick="currentSeriesChart.exportImageAsPng()">Export</button>'+
  '</div>\n'+
  '<div \n'+
  'style="position: absolute; width:10%; left: 0px; right: 0px; top: 40px; bottom: 0px;"'+
  '>\n'+ 
  this.propertyCheck_html()+
  '</div> \n'+
  '<div id="'+this.divId+'Chart" '+
//'style="width:100%;height:100%;"'+
//'style="float: right;"'+
  'style="position: absolute; left: 10%; right: 0px; top: 40px; bottom: 0px;"'+
  '>\n'+ 
  '</div>\n'+
  '<image id="imgExport" style="width:500px; height:300px; display:none;" >'+
  '</div>\n';
//console.log('TRACE htm='+htm);
  return(htm);
};
 

/////////////////////////////////////////////////////////////////////////////
seriesChart.prototype.show = function()
/////////////////////////////////////////////////////////////////////////////
{
  if (!(typeof(this.dygr.draw)==='undefined'))
  {
    this.active = true;
    $("#"+this.divId).show();
  //console.log('TRACE seriesChart.prototype.show');
    this.dygr.draw();
  }
};

/////////////////////////////////////////////////////////////////////////////
seriesChart.prototype.resize = function()
/////////////////////////////////////////////////////////////////////////////
{
//this.show();
};

/////////////////////////////////////////////////////////////////////////////
seriesChart.prototype.hide = function()
/////////////////////////////////////////////////////////////////////////////
{
  if (!(typeof(this.dygr.draw)==='undefined'))
  {
    this.active = false;
  //console.log('TRACE seriesChart.prototype.hide');
    $("#"+this.divId).hide();
  }
};

/////////////////////////////////////////////////////////////////////////////
seriesChart.prototype.onReady = function(env)
/////////////////////////////////////////////////////////////////////////////
{
//console.log('TRACE seriesChart.prototype.onReady('+this.name+')');
  var div = document.getElementById(this.container);
  div.insertAdjacentHTML('beforeend', this.html());
  document.getElementById(this.divId).style.fontSize = "16px";
  var this_ = this;
  env.ajax({
    url: this_.url,
    dataType: "json",
    error: function(result) {
  //console.log('TRACE ajax.error('+this_.name+','+this_.url+')');
    //console.log(this_);
    },
    success: function( json ) {
    //console.log('TRACE ajax.success('+this_.name+','+this_.url+')');
      this_.json = json;
    //console.log(json);
    //console.log(data);
      var options = 
      {
      //animatedZooms  : true,
      //displayAnnotations  : true,
        labels : this_.json.labels,
        legend: 'always',
        labelsDivStyles: { 'textAlign': 'right' },
        axes: {
          x: {
              valueFormatter: Dygraph.dateString_,
              axisLabelFormatter: Dygraph.dateAxisFormatter,
              ticker: Dygraph.dateTicker,
              gridLinePattern: [4,4]
          },
          y: {
              gridLinePattern: [4,4]
          }
        } /*          ,
        highlightCircleSize: 2,
        strokeWidth: 1,
        strokeBorderWidth: 1,
        highlightSeriesOpts: {
          strokeWidth: 3,
          strokeBorderWidth: 1,
          highlightCircleSize: 5
        } */
      //drawPoints : true,
      //rollPeriod: 1, showRoller: true,
      //customBars: true,
      };
      this_.propertyCheck_setElem(options);
      this_.dygr = new Dygraph(
        document.getElementById(this_.divId+'Chart'),
        this_.json.data,
        options 
      );
    }
  });  
//console.log(this_);
};


/////////////////////////////////////////////////////////////////////////////
seriesChart.prototype.remove = function()
/////////////////////////////////////////////////////////////////////////////
{
  $("#"+this.divId).remove();  
};


