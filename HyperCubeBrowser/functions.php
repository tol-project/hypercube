<?php

///////////////////////////////////////////////////////////////////////////////
function getTolExpression($datasets, $keyValues)
///////////////////////////////////////////////////////////////////////////////
{
  $ds = explode(',',$datasets);
  $kv = explode(',',$keyValues);
  echo "<html>\n";
  echo "<head>\n";
  echo "<meta charset='utf-8'>\n";
  echo "<title>Bayes HyperCube TOL Expression</title>\n";
  echo "<body>\n";
  echo "<h1>Bayes HyperCube TOL Expression</h1>\n";
  echo "<pre>\n";
  echo "<b>Set</b> series = { olap::getMatchingSeries(<br>\n";
  echo "&nbsp &nbsp Datasets = [[<br>\n";
  for ($i=0; $i < count($ds); $i++)
  {
    if($i>0) { echo ","; }
    echo "\n&nbsp &nbsp &nbsp &nbsp \"".$ds[$i]."\"";
  }
  echo "&nbsp &nbsp ]],<br>\n";
  echo "&nbsp &nbsp Keys = [[<br>\n";
  for ($i=0; $i < count($kv); $i++)
  {
    if($i>0) { echo ","; }
    echo "\n&nbsp &nbsp &nbsp &nbsp \"".$kv[$i]."\"";
  }
  echo "&nbsp &nbsp ]]<br>\n";
  echo ") };<br>\n";
  echo "</pre>\n";
  echo "</body>\n";
  echo "</html>\n";
};

///////////////////////////////////////////////////////////////////////////////
function getOneDatasetOneKeyValue($dbms, $dataset, $keyValue)
///////////////////////////////////////////////////////////////////////////////
{
//echo "TRACE getOneDatasetOneKeyValue\n  dataset=$dataset\n  keyValue=$keyValue\n";
  $query = "  
      SELECT B.id_serie 
      FROM def_serie_key as A 
      INNER JOIN def_serie as B
      ON A.id_serie = B.id_serie
      INNER JOIN def_dataset_dim as C 
        ON A.co_dataset = C.co_dataset
       AND A.co_dimension = C.co_dimension  
       AND A.co_dataset  = '".$dataset."'
       AND A.co_dimension || '.' || A.co_key = '".$keyValue."'
      GROUP BY B.id_serie";
  return($query);
};
  
///////////////////////////////////////////////////////////////////////////////
function getValidDimensions($dbms, $dataset, $keyValues)
///////////////////////////////////////////////////////////////////////////////
{
//echo "TRACE getValidDimensions\n  dataset=$dataset\n  keyValues=$keyValues\n";
  $kv = explode(',',$keyValues);
  $query = "";
  for ($i=0; $i < count($kv); $i++)
  {
    $key = $kv[$i];
    $dim = explode('.',$key)[0];
    if($i>0) { $query = $query . "UNION ALL\n"; }
    $query = $query . 
    "SELECT '".$key."' FROM def_dataset_dim ".
    "WHERE co_dataset = '".$dataset."' ".
    "AND co_dimension ='".$dim."'\n";
  }  
//echo "TRACE query=\n".$query;
  $table = $dbms->FetchTable($query);
  $vkv = [];
  for ($i=0; $i < count($table); $i++)
  {
    $vkv[$i] = $table[$i][0];
  }    
  return($vkv);
};

///////////////////////////////////////////////////////////////////////////////
function getOneDatasetAllKeyValues($dbms, $dataset, $keyValues)
///////////////////////////////////////////////////////////////////////////////
{
//echo "TRACE getOneDatasetAllKeyValues\n  dataset=$dataset\n  keyValues=$keyValues\n";
  $kv = getValidDimensions($dbms, $dataset, $keyValues);
//echo "TRACE count(kv)=".count($kv)."\n";
  $query = "
    SELECT id_serie FROM  def_serie
    WHERE ";
  for ($i=0; $i < count($kv); $i++)
  {
    $query = $query . "\n    ";
    if($i>0) { $query = $query . "AND "; }
    $query = $query . "id_serie IN  (" . 
       getOneDatasetOneKeyValue($dbms,$dataset,$kv[$i])."  )";
  }
  $query = $query . "\n  ";
  return($query);
};
  
///////////////////////////////////////////////////////////////////////////////
function getSeriesListSql($dbms, $datasets, $keyValues)
///////////////////////////////////////////////////////////////////////////////
{
//echo "TRACE getSeriesListSql\n  datasets=$datasets\n  keyValues=$keyValues\n";
  $ds = explode(',',$datasets);
//echo "TRACE count(ds)=".count($ds)."\n";
  $query = "
  SELECT id_serie, co_serie, co_dataset FROM  def_serie
  WHERE ";
  for ($i=0; $i < count($ds); $i++)
  {
    $query = $query . "\n  ";
    if($i>0) { $query = $query . "OR "; }
    $query = $query . "id_serie IN  (" . 
       getOneDatasetAllKeyValues($dbms,$ds[$i],$keyValues).")";
  }
  $query = $query . "\n  ";
  return($query);
};

///////////////////////////////////////////////////////////////////////////////
function getSeriesSummarySqlFromListQuery($dbms, $listQuery)
///////////////////////////////////////////////////////////////////////////////
{
//echo 'TRACE getSeriesSummarySqlFromListQuery listQuery=\n'.$listQuery;
  $query = 
  "SELECT  \n".
  "  co_serie,  \n".
  "  concat('avr:', cast(avg(vl_value) as float4),' min:',min(vl_value),' max:',max(vl_value)) as stats,  \n".
  "  concat(count(*),' ',dating,' data from ', min(dt_date),' to ', max(dt_date)) as \"time rank\", \n".
  "  description \n".
  "FROM(   \n".
  "SELECT  \n".
  "  A.co_serie,  \n".
  "  A.co_dataset,  \n".
  "  A.id_serie,  \n".
  "  count(*) as numReg, \n".
  "  ".$dbms->string_aggregate_fun()."(case when def_serie_key.co_dimension='Dating' then def_key.co_name else '' end,'') as dating, ".
  "  def_dataset.te_description ||  ".
  "  ', Units: ' || co_units_tol ||  ".
  "  ', Source: ' || co_source || ', ' || ".
  "  ".$dbms->string_aggregate_fun()."(def_serie_key.co_dimension||': '||def_serie_key.co_key||' ('||def_key.co_name||')', ', ') as description \n".
  "FROM  \n".
  "( \n".
  "--BEGIN listQuery \n".
  $listQuery. " \n".
  "--END listQuery "." \n".
  ") as A,  \n".
  "  def_serie_key,"." \n".
  "  def_key, \n".
  "  def_dataset \n".
  "WHERE  \n".
  "  A.id_serie = def_serie_key.id_serie AND \n".
  "  A.co_dataset = def_serie_key.co_dataset AND \n".
  "  A.co_dataset = def_dataset.co_dataset AND \n".
  "  def_serie_key.co_dimension = def_key.co_dimension AND \n".
  "  def_serie_key.co_key = def_key.co_key  \n".
  "GROUP BY A.co_serie, A.co_dataset, A.id_serie, co_source, co_units_tol, def_dataset.te_description \n".
  ")as A, \n".
  "dat_serie as B \n".
  "WHERE A.id_serie = B.id_serie  \n".
  "GROUP BY co_serie, co_dataset, description, dating \n".
  "ORDER BY co_serie \n";
//echo 'TRACE getSeriesSummarySqlFromListQuery query=\n'.$query;
  return($query);
};

///////////////////////////////////////////////////////////////////////////////
function getSeriesSummarySql($dbms, $datasets, $keyValues)
///////////////////////////////////////////////////////////////////////////////
{
  $query = getSeriesSummarySqlFromListQuery($dbms,
    getSeriesListSql($dbms,$datasets, $keyValues));
  return($query);
};

///////////////////////////////////////////////////////////////////////////////
function getSeriesDataFromQuery($dbms,$query)
///////////////////////////////////////////////////////////////////////////////
{
//echo "TRACE getSeriesDataFromQuery\n  query=\n$query\n";
  $table =$dbms->FetchTable($query);
  $num_ser = count($table);
  if(!$num_ser) { return("SELECT now(), null as NoMatchingSeries"); } 
  $selectVal = "";
  $from = " 
  FROM (SELECT dt_date FROM dat_serie WHERE id_serie in(";
  for ($i=0; $i < $num_ser; $i++)
  {
    $idSer = $table[$i][1];
    if($i>0) { $from = $from.", "; }
    $from = $from.$idSer;
  }
  $from = $from.") GROUP BY dt_date) as S0
  "; 
  $coSer = "";
  $idSer = 0;
  for ($i=0; $i < $num_ser; $i++)
  {
    $coSer = $table[$i][0];
    $idSer = $table[$i][1];
    $i1 = $i+1;
    $selectVal = $selectVal.",
    S".$i1.".vl_value as \"".$coSer."\" ";    
    $from = $from."LEFT OUTER JOIN ".
    "  (SELECT * FROM dat_serie WHERE id_serie =".$idSer.") as S".$i1." ".
    "  ON S0.dt_date = S".$i1.".dt_date 
  ";    
  }
  $select = "
  SELECT S0.dt_date as dt_date".$selectVal.$from."ORDER BY S0.dt_date; 
  ";
  return($select);
};


///////////////////////////////////////////////////////////////////////////////
function getSeriesDataSqlFromCodes($dbms,$serCodes)
///////////////////////////////////////////////////////////////////////////////
{
  $sc = str_replace(",","','",$serCodes);
  $query = "
  SELECT co_serie, id_serie, co_dataset
  FROM def_serie
  WHERE co_serie in ('".$sc."')
  ";
  return(getSeriesDataFromQuery($dbms,$query));
};

///////////////////////////////////////////////////////////////////////////////
function getSeriesDataSqlFromCross($dbms,$datasets, $keyValues)
///////////////////////////////////////////////////////////////////////////////
{
  $query = getSeriesListSql($dbms,$datasets, $keyValues);
  return(getSeriesDataFromQuery($dbms,$query));
};

///////////////////////////////////////////////////////////////////////////////
function getFieldTypes($dbms)
///////////////////////////////////////////////////////////////////////////////
{
  $fld_num = $dbms->fieldNumber;
  $types=array();
  for ($j=0; $j < $fld_num; $j++)
  {
    $type =$dbms->fieldTypes[$j];
    switch ($type) {
    case 'date'      : $types[] = 'date'; break;
    case 'timestamp' : $types[] = 'datetime'; break;
    case 'boolean'   : $types[] = 'number';   break;
    case 'int2'      : $types[] = 'number';   break;
    case 'int4'      : $types[] = 'number';   break;
    case 'int8'      : $types[] = 'number';   break;
    case 'integer'   : $types[] = 'number';   break;
    case 'float'     : $types[] = 'number';   break;
    case 'double'    : $types[] = 'number';   break;
    case 'float4'    : $types[] = 'number';   break;
    case 'float8'    : $types[] = 'number';   break;
    case 'text'      : $types[] = 'string';   break;
    case 'varchar'   : $types[] = 'string';   break;
    default          : $types[] = $type; }
  }  
  return($types);
};  
 
///////////////////////////////////////////////////////////////////////////////
function getFormattedCell($handler, $type, $fval)
///////////////////////////////////////////////////////////////////////////////
{ 
  switch ($type) {
  case 'date':
  case 'datetime' : 
  {
    if($handler=='DataTable')
    {
      return(json_encode($fval));
    }
    $len = strlen($fval);
    if($len==10)
    {
      sscanf($fval, '%d-%d-%d', 
             $year, $month, $day);
      if($handler=='DyGraphCsv')
      {
        return("".$year."/".$month."/".$day);
      }
      else if($handler=='DyGraphNative')
      {
        $date = new DateTime($fval);
        return($date->getTimestamp()*1000);
       //return("Date($year, $month, $day)");
      }
      else
      {
        return("\"Date($year, $month, $day)\"");
      }
    }
    else if($len==12)
    {
      sscanf($fval, '%d-%d-%d %d:%d:%d', 
             $year, $month, $day, $hour, $minute, $second);
      if($handler=='DyGraphCsv')
      {
        return("".$year."/".$month."/".$day." ".$hour.":".$minute.":".$second);
      }
      else
      {
        return("\"Date($year, $month, $day, $hour, $minute, $second)\"");
      }
    }
  };
  case 'number' : 
  {
    return(''.(is_null($fval)?($handler=='DyGraphCsv'?'':'null'):round($fval,4)).''); 
  }
  default       : return(json_encode($fval));        }
};

///////////////////////////////////////////////////////////////////////////////
function query2jsonGoogleVisualization($dbms,$query)
///////////////////////////////////////////////////////////////////////////////
{
  $table = $dbms->FetchTable($query);
  $reg_num = count($table);
  $fld_num =$dbms->fieldNumber;
  $types= getFieldTypes($dbms);
 
  $jsonRows = "  \"rows\":[";
  for ($i=0; $i < $reg_num; $i++)
  {
    if($i>0) { $jsonRows = $jsonRows .  ','; }
    $jsonRows = $jsonRows .  "\n    {\"c\":[";
    for ($j=0; $j < $fld_num; $j++)
    {
      $fval = $table[$i][$j];      
      if($j>0) { $jsonRows = $jsonRows .  ','; }
      $jsonRows = $jsonRows .  "\n      {\"v\":";
      $jsonRows = $jsonRows . getFormattedCell('GoogleVisualization',$types[$j], $fval);
      $jsonRows = $jsonRows .  "}";
    }    
    $jsonRows = $jsonRows .  "\n    ] }";
  }
  $jsonRows = $jsonRows .  "\n  ]";
//echo("TRACE [query2json] jsonRows=".$jsonRows);
  
  $jsonCols = "  \"cols\":[";
  for ($j=0; $j < $fld_num; $j++)
  {
    if($j>0) { $jsonCols = $jsonCols . ','; }
    $jsonCols = $jsonCols .  "\n    {\"id\":\"F".$j."\", \"label\":\"".$dbms->fieldNames[$j]."\", \"type\":\"".$types[$j]."\"}";
  }  
  $jsonCols = $jsonCols .  "\n  ]";
//echo("TRACE [query2json] jsonCols=".$jsonCols);
  $json = "{\n".$jsonCols.",\n".$jsonRows."\n}\n";
//echo("TRACE [query2json] json=".$json);
  return($json);
};

///////////////////////////////////////////////////////////////////////////////
function query2jsonDataTable($dbms,$query)
///////////////////////////////////////////////////////////////////////////////
{
  $table = $dbms->FetchTable($query);
  $reg_num = count($table);
  $fld_num =$dbms->fieldNumber;
  $types= getFieldTypes($dbms);
  
  $jsonRows = "  \"data\":[";
  for ($i=0; $i < $reg_num; $i++)
  {
    if($i>0) { $jsonRows = $jsonRows .  ','; }
    $jsonRows = $jsonRows .  "\n    [";
    for ($j=0; $j < $fld_num; $j++)
    {
      $fval = $table[$i][$j];      
      if($j>0) { $jsonRows = $jsonRows .  ', '; }
      switch ($types[$j]) {
      case 'number' : $jsonRows = $jsonRows .  ''.(is_null($fval)?'null':round($fval,4)).''; break;
      default       : $jsonRows = $jsonRows .  json_encode($fval);        }
    }
    $jsonRows = $jsonRows .  "]";
  }
  $jsonRows = $jsonRows .  "\n  ]";
//echo("TRACE [query2json] jsonRows=".$jsonRows);
  
  $jsonCols = "  \"columns\":[";
  for ($j=0; $j < $fld_num; $j++)
  {
    if($j>0) { $jsonCols = $jsonCols . ','; }
    $jsonCols = $jsonCols .  "\n    {\"title\":\"".$dbms->fieldNames[$j]."\"}";
  }  
  $jsonCols = $jsonCols .  "\n  ]";
//echo("TRACE [query2json] jsonCols=".$jsonCols);
  $json = "{\n".$jsonCols.",\n".$jsonRows. "\n}\n";
//echo("TRACE [query2json] json=".$json);
  return($json);
};

///////////////////////////////////////////////////////////////////////////////
function query2jsonDyGraph($dbms,$query)
///////////////////////////////////////////////////////////////////////////////
{
  $table = $dbms->FetchTable($query);
  $reg_num = count($table);
  $fld_num =$dbms->fieldNumber;
  $types= getFieldTypes($dbms);
  
  $jsonRows = "  \"data\":[";
  for ($i=0; $i < $reg_num; $i++)
  {
    if($i>0) { $jsonRows = $jsonRows .  ','; }
    $jsonRows = $jsonRows .  "\n    [";
    for ($j=0; $j < $fld_num; $j++)
    {
      $fval = getFormattedCell('DyGraphNative', $types[$j], $table[$i][$j]);      
      if($j>0) { $jsonRows = $jsonRows .  ', '; }
      $jsonRows = $jsonRows .  $fval;
    }
    $jsonRows = $jsonRows .  "]";
  }
  $jsonRows = $jsonRows .  "\n  ]";
//echo("TRACE [query2json] jsonRows=".$jsonRows);
  
  $jsonCols = "  \"labels\":[";
  for ($j=0; $j < $fld_num; $j++)
  {
    if($j>0) { $jsonCols = $jsonCols . ','; }
    $jsonCols = $jsonCols .  "\n    \"".$dbms->fieldNames[$j]."\"";
  }  
  $jsonCols = $jsonCols .  "\n  ]";
//echo("TRACE [query2json] jsonCols=".$jsonCols);
  $json = "{\n".$jsonCols.",\n".$jsonRows. "\n}\n";
//echo("TRACE [query2json] json=".$json);
  return($json);
};

///////////////////////////////////////////////////////////////////////////////
function query2json($dbms, $tabEngine, $query)
///////////////////////////////////////////////////////////////////////////////
{
  if($tabEngine=='GoogleVisualization')
  {
     return(query2jsonGoogleVisualization($dbms,$query));
  }
  if($tabEngine=='DataTable')
  {
     return(query2jsonDataTable($dbms,$query));
  }
  if($tabEngine=='DyGraphNative')
  {
     return(query2jsonDyGraph($dbms,$query));
  }
}

///////////////////////////////////////////////////////////////////////////////
function echoJsonFromQuery($dbms, $tabEngine, $query)
///////////////////////////////////////////////////////////////////////////////
{
  echo query2json($dbms, $tabEngine, $query);
};

///////////////////////////////////////////////////////////////////////////////
function query2csv($dbms, $tabSep, $query)
///////////////////////////////////////////////////////////////////////////////
{
//echo "TRACE query2csv query=\n".$query."<br>\n";
  $table = $dbms->FetchTable($query);
  $reg_num = count($table);
  $fld_num =$dbms->fieldNumber;
  $types= getFieldTypes($dbms);
//echo "TRACE query2csv fld_num=\n".$fld_num."<br>\n";
//echo "TRACE query2csv reg_num=\n".$reg_num."<br>\n";
  $csv = ""; 
  for ($j=0; $j < $fld_num; $j++)
  {
    if($j>0) { $csv = $csv . $tabSep; }
    $csv = $csv .  $dbms->fieldNames[$j];
  } 
  $csv = $csv . "\n";  
  for ($i=0; $i < $reg_num; $i++)
  {
    for ($j=0; $j < $fld_num; $j++)
    {
      $fval = getFormattedCell('DyGraphCsv', $types[$j], $table[$i][$j]);      
      if($j>0) { $csv = $csv .  $tabSep . ' '; }
      $csv = $csv .  $fval;
    }
    $csv = $csv . "\n";  
  } 
  return($csv);
};

///////////////////////////////////////////////////////////////////////////////
function echoCsvFromQuery($dbms, $tabSep, $query)
///////////////////////////////////////////////////////////////////////////////
{
  echo(query2csv($dbms, $tabSep, $query));
};


///////////////////////////////////////////////////////////////////////////////
function strToHex($string)
///////////////////////////////////////////////////////////////////////////////
{
    $hex='';
    for ($i=0; $i < strlen($string); $i++)
    {
        $hex .= dechex(ord($string[$i]));
    }
    return $hex;
}

///////////////////////////////////////////////////////////////////////////////
function hexToStr($hex)
///////////////////////////////////////////////////////////////////////////////
{
    $string='';
    for ($i=0; $i < strlen($hex)-1; $i+=2)
    {
        $string .= chr(hexdec($hex[$i].$hex[$i+1]));
    }
    return $string;
}

///////////////////////////////////////////////////////////////////////////////
function encrypt_ccc($crypt, $decripted, $key) 
///////////////////////////////////////////////////////////////////////////////
{ 
//echo "    <!-- TRACE encrypt_ccc: decripted = $decripted,  key = $key -->\n";
//$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $decripted, MCRYPT_MODE_CBC, md5(md5($key))));
//$encrypted = base64_encode(convert_uuencode($key . $decripted . $key)); 
  $encrypted = $crypt->encrypt($key, $decripted, 16);
//echo "    <!-- TRACE encrypt_ccc: encrypted = $encrypted -->\n";
  $encrypted = strToHex($encrypted);
//echo "    <!-- TRACE encrypt_ccc: encrypted = $encrypted -->\n";
  return $encrypted; 
};

///////////////////////////////////////////////////////////////////////////////
function decrypt_ccc($crypt, $encrypted, $key) 
///////////////////////////////////////////////////////////////////////////////
{ 
//echo "    <!-- TRACE decrypt_ccc: encrypted = $encrypted,  key = $key -->\n";
  $encrypted = hexToStr($encrypted);   
//echo "    <!-- TRACE decrypt_ccc: encrypted = $encrypted -->\n";
//$decripted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
//$decripted = str_replace($password,'',convert_uudecode(base64_decode($key))); 
  $decripted = $crypt->decrypt($key, $encrypted); 
//echo "    <!-- TRACE decrypt_ccc: decripted = $decripted -->\n";
  return $decripted;
};

///////////////////////////////////////////////////////////////////////////////
function trace_die($msg) 
///////////////////////////////////////////////////////////////////////////////
{ 
  if($DO_TRACE) {
    echo "  <head>\n";
    echo "    <!-- TRACE trace_die -->\n";
    echo "    <!-- TRACE serverName: $serverName -->\n";
    echo "    <!-- TRACE requestUrl: $requestUrl -->\n";
    echo "    <!-- TRACE httpReferer: $httpReferer -->\n";
    echo "    <!-- TRACE remoteAddr: $remoteAddr -->\n";
    echo "    <!-- TRACE remoteHost: $remoteHost -->\n";
    echo "    <!-- TRACE notes: $notes -->\n";
    echo "  </head>\n";
    echo "</html>\n";
  };
  return(die($msg));
};

///////////////////////////////////////////////////////////////////////////////
function pre_code($str) 
///////////////////////////////////////////////////////////////////////////////
{
  echo "\n<pre><code>\n".
  str_replace(">","&gt;", str_replace("<","&lt;", $str)).
  "\n</code></pre>";
};
?>